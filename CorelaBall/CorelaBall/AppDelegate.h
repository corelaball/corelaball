//
//  AppDelegate.h
//  CorelaBall
//
//  Created by Christiana Tebbs on 11/29/18.
//  Copyright © 2018 Christiana Tebbs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


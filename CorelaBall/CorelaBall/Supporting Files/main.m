//
//  main.m
//  CorelaBall
//
//  Created by Christiana Tebbs on 11/29/18.
//  Copyright © 2018 Christiana Tebbs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

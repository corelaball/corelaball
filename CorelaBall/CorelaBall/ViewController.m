//
//  ViewController.m
//  CorelaBall
//
//  Created by Christiana Tebbs on 11/29/18.
//  Copyright © 2018 Christiana Tebbs. All rights reserved.
//

#import "ViewController.h"
#import "NSArray+IOC.h"
#import "IOCImageAndTextViewSlideShow.h"
#import "IOCTextInput.h"


@import CoreMotion;
@import AVFoundation;
@interface ViewController () <UITextFieldDelegate>

@property (strong, nonatomic) NSMutableArray *baskets;
@property (weak, nonatomic) IBOutlet UILabel *gameOver;
@property (weak, nonatomic) IBOutlet UIButton *restartButton;
@property (weak, nonatomic) IBOutlet UIImageView *bin1; //Compost bin
@property (weak, nonatomic) IBOutlet UIImageView *bin2; //Recycling bin
@property (weak, nonatomic) IBOutlet UIImageView *bin3; //Trash bin
@property (weak, nonatomic) IBOutlet UIImageView *bin4; //"I don't know" Bin
@property (weak, nonatomic) IBOutlet UIImageView *lifeOne;
@property (weak, nonatomic) IBOutlet UIImageView *lifeTwo;
@property (weak, nonatomic) IBOutlet UIImageView *lifeThree;
@property (nonatomic) float changingSpeed;
@property (weak, nonatomic) IBOutlet UIView *Detect1;
@property (weak, nonatomic) IBOutlet UIView *Detect2;
@property (weak, nonatomic) IBOutlet UIView *Detect3;
@property (weak, nonatomic) IBOutlet UIView *Detect4;
@property (strong, nonatomic) NSArray *gameImages; //An array to hold the images we put in to be sorted
@property (strong, nonatomic) NSMutableArray *gameImagesImageViews; //An array to use for the image views of the gameImages

//Physics related properties
@property (strong, nonatomic) UIDynamicAnimator *animator;
@property (strong, nonatomic) UIGravityBehavior *gravityBehavior;
@property (strong, nonatomic) UICollisionBehavior *collisionBehavior;

// device motion
@property (strong, nonatomic) CMMotionManager *motionManager;
@property (strong, nonatomic) CMDeviceMotion *deviceMotion; //Device motion is not used currently in this app, instead we are using motionmanager. Figuring out how to use device motion might help translate the object only if we are using the value to tilt side to side (our end goal)

// KEEP SCORE
@property (weak, nonatomic) IBOutlet UIView *vImagePreview;
@property (nonatomic) int currentScore;

@property (nonatomic) int numLives;
@property (nonatomic) int topScore;
@property (weak, nonatomic) IBOutlet UILabel *topScoreLabel;
@property (nonatomic) NSString *objectType;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;

//we should add another property here to keep track of the lives we have left
//and more properties that are connected to the life counter imageViews on our storyboard
@property (weak, nonatomic) IBOutlet UILabel *testLabel;
@end


@implementation ViewController


// in order to do Dynamic Animation you follow these steps:
//
//      1) create a dynamic animator
//      2) create the behaviors you want and add them to the animator
//      3) associate items (UIViews or subclasses - in our case, UILabels) to the behaviors


#pragma mark - lazy instantiation

//Lazy instantiation means that these methods are all called only when these items are used (instead of declaring them in setup at the start)

- (NSArray *)gameImages
{
    if (!_gameImages) {
        _gameImages = [NSArray arrayWithImageFilesUsingPrefix:@"sort" startingNumber:1 toNumber:15 withNumDigits:3];
    }
    return _gameImages;
}

- (NSMutableArray *)gameImagesImageViews
{
    if (!_gameImagesImageViews) {
        _gameImagesImageViews = [[NSMutableArray alloc] init];
    }
    return _gameImagesImageViews;
    
}

- (UIDynamicAnimator *)animator
{
    if (!_animator) {
        _animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    }
    return _animator;
}

- (UIGravityBehavior *)gravityBehavior
{
    if (!_gravityBehavior) {
        _gravityBehavior = [[UIGravityBehavior alloc] init];
        [self.animator addBehavior:_gravityBehavior];
    }
    return _gravityBehavior;
}

- (UICollisionBehavior *)collisionBehavior
{
    if (!_collisionBehavior) {
        _collisionBehavior = [[UICollisionBehavior alloc] init];
        _collisionBehavior.translatesReferenceBoundsIntoBoundary = YES;
        _collisionBehavior.collisionDelegate = self;
        [self.animator addBehavior:_collisionBehavior];
    }
    return _collisionBehavior;
}

- (CMDeviceMotion *)deviceMotion
{
    if (!_deviceMotion) {
        _deviceMotion = self.motionManager.deviceMotion;
    }
    return _deviceMotion;
}

- (CMMotionManager *)motionManager
{
    if (!_motionManager) {
        _motionManager = [[CMMotionManager alloc] init];
        _motionManager.deviceMotionUpdateInterval = .016;
    }
    return _motionManager;
}

#pragma mark - setup
- (void)setupDeviceMotion  //Sets up the object moving as the device moves. Works both side to side and up and down
{
    // this method will call the block you pass in at the interval that we define
    // in our lazy instantiation of the motionManager
    
    [self.motionManager startDeviceMotionUpdatesToQueue:[NSOperationQueue mainQueue] withHandler:^(CMDeviceMotion * _Nullable motion, NSError * _Nullable error) {              //motionmanager we set up earlier with CMMotionManager
        
        // here we associate the device's gravity vector with our gravity behavior
        // in the physics simulation
        CMAcceleration gravity = motion.gravity;
        //        self.gravityBehavior.gravityDirection = CGVectorMake(gravity.x, -gravity.y); //We later put gravity behavior on the objects, which is what allows them to move using this CMAccelaration part of CMMotionManager
        //NSLog(@"You have reached Device motion setup");
        self.gravityBehavior.gravityDirection = CGVectorMake(gravity.x, .2); //We later put gravity behavior on the objects, which is what allows them to move using this CMAccelaration part of CMMotionManager
        
        
        
        // we aren't using hte deviceMotion property in this example
        // but it's here so you can see how you might use it.
        self.deviceMotion = motion;
        float pitch = self.deviceMotion.attitude.pitch;
        float yaw = self.deviceMotion.attitude.yaw;
        float roll = self.deviceMotion.attitude.roll; //Tilting phone side to side, useful for having object move when we tilt phone
        
        // NSLog(@"pitch: %.2f | yaw: %.2f | roll: %.2f", pitch, yaw, roll);
        
    }];
}
-(void)setup
{
    
    [self.view bringSubviewToFront:self.bin1];
    [self.view bringSubviewToFront:self.bin2];
    [self.view bringSubviewToFront:self.bin3];
    [self.view bringSubviewToFront:self.bin4];
    
    self.restartButton.hidden = YES;
    self.testLabel.hidden = YES;
    self.changingSpeed = .02;
    self.currentScore = 0;
    self.numLives = 3;
    self.topScore = 0;
    [self updateScoreLabel];
}

- (void)updateScoreLabel
{
    NSString *currentScoreAsString = [NSString stringWithFormat:@"%i", self.currentScore];
    self.scoreLabel.text = currentScoreAsString;
}

- (IBAction)resetGame:(id)sender {
    [self createNewObject];
    self.numLives = 3;
    self.lifeOne.hidden = NO;
    self.lifeTwo.hidden = NO;
    self.lifeThree.hidden = NO;
    self.currentScore = 0;
    [self updateScoreLabel];
    self.testLabel.hidden = YES;
    self.restartButton.hidden = YES;
}

-(void)createNewObject
{
    UIImage *randomImage = [self.gameImages getRandomObject];
    if(randomImage == self.gameImages[1] || randomImage == self.gameImages[5] || randomImage == self.gameImages[6] || randomImage == self.gameImages[9]){
        NSLog(@"Created a Compostable Object");
        self.objectType = @"Compost";
    } else if(randomImage == self.gameImages[0] || randomImage == self.gameImages[2] || randomImage == self.gameImages[3] || randomImage == self.gameImages[13] || randomImage == self.gameImages[14]){
        NSLog(@"Created a Recyclable Object");
        self.objectType = @"Recycle";
    } else if(randomImage == self.gameImages[4] || randomImage == self.gameImages[7] || randomImage == self.gameImages[8] || randomImage == self.gameImages[10] || randomImage == self.gameImages[11] || randomImage == self.gameImages[12]){
        NSLog(@"Created a trash object");
        self.objectType = @"Trash";
    }
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:randomImage];
    imageView.frame = CGRectMake(150, 10, 70, 70);
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.gameImagesImageViews addObject:imageView];
    [self.view addSubview:imageView];
    [self.view sendSubviewToBack:imageView];
    [self addImageViewToAnimationBehaviors:imageView];
    [self.collisionBehavior addItem:imageView];
}

-(void)setupBaskets
{
    self.baskets = [NSMutableArray arrayWithObjects: self.Detect1, self.Detect2, self.Detect3, self.Detect4, nil];
    UIDynamicItemBehavior *customBehavior = [[UIDynamicItemBehavior alloc] init];
    customBehavior.density = 100000.0;
    [self.animator addBehavior:customBehavior];
    
    // ADD EACH PART OF BASKET TO CUSTOM BEHAVIOR
    
    for (NSUInteger i = 0; i < self.baskets.count; i++) {
        UIImageView *thisView = [self.baskets objectAtIndex:i];
        [self.view addSubview:thisView];
        [self.collisionBehavior addItem:thisView];
        [customBehavior addItem:thisView];
    }
}

#pragma mark - collision delegate methods
- (void)collisionBehavior:(UICollisionBehavior *)behavior endedContactForItem:(id <UIDynamicItem>)item1 withItem:(id <UIDynamicItem>)item2
{
    NSLog(@"Collision detected!");
    [self setupDeviceMotion];
    NSArray *gameImageImageViewsImmutable = [NSArray arrayWithArray:self.gameImagesImageViews];
    
    //In this if statements, create code to determine what object it's colliding with, and if that object has entered the correct bin.
    //Go through the array 'gameImages' to do this (?)
    //Also, ordering game images so that the items of each type are grouped (All compost being first, all recycling being second, and all trash being third) will help so we only have to have if statements for certain indexes (for instance items with indexes 1 through 4 could be compost)
    
    
    if ([item1 isEqual: self.baskets[0]] || [item2 isEqual: self.baskets[0]]){ //Compost
        // NSLog(@"Collision detected with Compost");
        if([self.objectType isEqualToString:@"Compost"]){
            //    self.testLabel.text = @"Correctly Composted!";
        } else{
            //    self.testLabel.text = @"Incorrectly Composted";
            self.currentScore = 0;
            [self updateScoreLabel];
            if(self.numLives == 3){
                self.lifeOne.hidden = YES;
                self.numLives--;
            } else if(self.numLives == 2){
                self.lifeTwo.hidden = YES;
                self.numLives--;
            } else if(self.numLives == 1){
                self.lifeThree.hidden = YES;
                self.numLives--;
            } else if(self.numLives == 0){
                self.lifeOne.hidden = NO;
                self.lifeTwo.hidden = NO;
                self.lifeThree.hidden = NO;
                self.numLives = 3;
                self.currentScore = 0;
                [self updateScoreLabel];
            }
        }
    }
    
    if ([item1 isEqual: self.baskets[1]] || [item2 isEqual: self.baskets[1]]){ //Recycling
        NSLog(@"Collision detected with Recycling");
        // self.testLabel.text = @"Recycling";
        if([self.objectType isEqualToString:@"Recycle"]){
            //  self.testLabel.text = @"Correctly Recycled!";
        } else{
            //  self.testLabel.text = @"Incorrectly Recycled";
            self.currentScore = 0;
            [self updateScoreLabel];
            if(self.numLives == 3){
                self.lifeOne.hidden = YES;
                self.numLives--;
            } else if(self.numLives == 2){
                self.lifeTwo.hidden = YES;
                self.numLives--;
            } else if(self.numLives == 1){
                self.lifeThree.hidden = YES;
                self.numLives--;
            } else if(self.numLives == 0){
                self.lifeOne.hidden = NO;
                self.lifeTwo.hidden = NO;
                self.lifeThree.hidden = NO;
                self.numLives = 3;
                self.currentScore = 0;
                [self updateScoreLabel];
            }
        }
    }
    
    if ([item1 isEqual: self.baskets[2]] || [item2 isEqual: self.baskets[2]]){ //Trash
        NSLog(@"Collision detected with Trash");
        //  self.testLabel.text = @"Trash";
        if([self.objectType isEqualToString:@"Trash"]){
            //      self.testLabel.text = @"Correctly Trashed!";
        } else{
            //    self.testLabel.text = @"Incorrectly Trash";
            self.currentScore = 0;
            [self updateScoreLabel];
            if(self.numLives == 3){
                self.lifeOne.hidden = YES;
                self.numLives--;
            } else if(self.numLives == 2){
                self.lifeTwo.hidden = YES;
                self.numLives--;
            } else if(self.numLives == 1){
                self.lifeThree.hidden = YES;
                self.numLives--;
            } else if(self.numLives == 0){
                self.lifeOne.hidden = NO;
                self.lifeTwo.hidden = NO;
                self.lifeThree.hidden = NO;
                self.numLives = 3;
                self.currentScore = 0;
                [self updateScoreLabel];
            }
        }
    }
    
    if ([item1 isEqual: self.baskets[3]] || [item2 isEqual: self.baskets[3]]){ //"I don't know"
        NSLog(@"Collision detected with I don't know");
        // self.testLabel.text = @"IDK";
        //Add a way to put the image back on the top of the screen, because once hitting this bin it should 'pop back out' after displaying text to give the user a hint about which bin is correct
    }
    
    for (UIView *view in self.baskets) {
        
        if ([item1 isEqual:view] || [item2 isEqual:view]) {
            
            NSLog(@"collision detected with a basket");
            
            for (UIImageView *gameImageView in gameImageImageViewsImmutable) {
                //  for (NSValue *rectValue in self.baskets) {
                // self.changingSpeed += .01;
                //     NSLog(@"%f", self.changingSpeed);
                //   [self setupDeviceMotion];
                if(self.numLives != 0){
                    [self removeImageViewFromGame:gameImageView];
                    self.currentScore++;
                    if(self.currentScore > self.topScore){
                        self.topScore = self.currentScore;
                        self.topScoreLabel.text = [NSString stringWithFormat:@"%i", self.topScore];
                    }
                    [self updateScoreLabel];
                    [self createNewObject];
                } else{
                    [self removeImageViewFromGame:gameImageView];
                    self.restartButton.hidden = NO;
                    self.testLabel.hidden = NO;
                }
                //}
            }
        }
    }
}

- (void)addImageViewToAnimationBehaviors:(UIImageView *)imageView
{
    [self.gravityBehavior addItem:imageView];
    [self.collisionBehavior addItem:imageView];
}

- (void)removeImageViewFromGame:(UIImageView *)imageView
{
    [self.gravityBehavior removeItem:imageView];
    [self.collisionBehavior removeItem:imageView];
    [imageView removeFromSuperview];
    [self.gameImagesImageViews removeObject:imageView];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
    [self setupDeviceMotion];
    [self createNewObject];
    [self setupBaskets];
    // Do any additional setup after loading the view, typically from a nib.
}
@end

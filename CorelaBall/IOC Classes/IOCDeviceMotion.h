//
//  IOCDeviceMotion.h
//  IOCDeviceMotionExample
//
//  Created by Abraham Avnisan on 4/11/17.
//  Copyright © 2017 Abraham Avnisan. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol IOCDeviceMotionDelegate;

/**
 * @class IOCDeviceMotion
 * @discussion  This class allows you to activate the device's accelerometers
 *              so that your app can respond to the device's pitch, yaw and roll.
 *              In order to use it, your ViewController class should implement
 *              an instance method called update (- (void)update{ //update code goes here... })
 *              The update method will be called automatically by the IOCDeviceMotionDelegate
 *              object, and in that method you can use the dot notation to access the object's
 *              pitch, yaw and roll properties.
 */
@interface IOCDeviceMotion : NSObject

/** @brief the device's current pitch in degrees */
@property (nonatomic) float degreesPitch;
/** @brief the device's current yaw in degrees */
@property (nonatomic) float degreesYaw;
/** @brief the device's current roll in degrees */
@property (nonatomic) float degreesRoll;

/**
 * @discussion  This is the designated initializer method for this class.
 *              There are two steps you need to take in order to properly
 *              initialize this class. FIRST, you must make declare that
 *              your ViewController class conforms to the IOCDeviceMotionDelegate
 *              protocol by changing this line in your ViewController.h file
 *              "@interface ViewController : UIViewController" to 
 *              "@interface ViewController : UIViewController <IOCDeviceMotionDelegate>"
 *              SECOND, in your ViewController's setup method, you need to initialize your
 *              object like so (assuming you've declared a property called deviceMotion to
 *              hold an instance of this object):
 *              self.deviceMotion = [[IOCDeviceMotion alloc] initWithDelegate:self];
 */
- (instancetype)initWithDelegate:(id<IOCDeviceMotionDelegate>)delegate;

@end






// (DON'T WORRY ABOUT THIS)
// ________________________________________________________________________________________________ //
@protocol IOCDeviceMotionDelegate <NSObject>
@optional
- (void)update;
@end
// ________________________________________________________________________________________________ //


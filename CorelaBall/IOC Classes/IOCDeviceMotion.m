//
//  IOCDeviceMotion.m
//  IOCDeviceMotionExample
//
//  Created by Abraham Avnisan on 4/11/17.
//  Copyright © 2017 Abraham Avnisan. All rights reserved.
//

#import "IOCDeviceMotion.h"
#import "IOCMath.h"

@import CoreMotion;
@interface IOCDeviceMotion ()

@property (strong, nonatomic) CMDeviceMotion *deviceMotion;
@property (strong, nonatomic) CMMotionManager *motionManager;
@property (strong, nonatomic) IOCMath *mathHelper;
@property (strong, nonatomic) id<IOCDeviceMotionDelegate> delegate;


@end

@implementation IOCDeviceMotion
#pragma mark - lazy instantiation & setters & getters
- (IOCMath *)mathHelper
{
    if (!_mathHelper) {
        _mathHelper = [[IOCMath alloc] init];
    }
    return _mathHelper;
}
- (void)setDeviceMotion:(CMDeviceMotion *)deviceMotion
{
    _deviceMotion = deviceMotion;
    
    self.degreesYaw = [self.mathHelper convertRadiansToDegrees:deviceMotion.attitude.yaw];
    self.degreesRoll = [self.mathHelper convertRadiansToDegrees:deviceMotion.attitude.roll];
    self.degreesPitch = [self.mathHelper convertRadiansToDegrees:deviceMotion.attitude.pitch];
}
#pragma mark - designated initializer
- (instancetype)initWithDelegate:(id<IOCDeviceMotionDelegate>)delegate
{
    self = [super init];
    if (self) {
        self.delegate = delegate;
        self.motionManager = [[CMMotionManager alloc] init];
        [self.motionManager startDeviceMotionUpdatesUsingReferenceFrame:CMAttitudeReferenceFrameXMagneticNorthZVertical
                                                                toQueue:[NSOperationQueue mainQueue]
                                                            withHandler:^(CMDeviceMotion * _Nullable motion, NSError * _Nullable error) {
                                                              
                                                                self.deviceMotion = motion;
                                                                [self.delegate update];
                                                                
                                                            }];
        
        self.motionManager.deviceMotionUpdateInterval = 1.0/30.0;

    }
    return self;
}

@end

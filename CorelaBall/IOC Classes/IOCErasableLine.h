//
//  IOCErasableLine.h
//  Week7CodingChallengeSolution
//
//  Created by Abraham Avnisan on 3/15/17.
//  Copyright © 2017 Abraham Avnisan. All rights reserved.
//

#import <Foundation/Foundation.h>
/**
 * @class IOCErasableLine
 * @discussion a class that allows you to create an "erasable line" - a line whose characters can be erased based on intensities set on the left, center, and right-hand side of the line
 */
@interface IOCErasableLine : NSObject

// PROPERTIES
@property (nonatomic, strong) NSArray *words;               // array of erasable words
@property (nonatomic, strong) NSArray *wordsAsStringArray;

/** @brief the erased line */
@property (strong, nonatomic) NSString *line;

// METHODS

/**
 * @discussion this is the required init method for this class
 * @param line the complete line you want to erase
 * @param intensityLeft the intensity of erasure for the left side of the line (a float from 0.0 - 1.0)
 * @param intensityCenter the intensity of erasure for the center side of the line (a float from 0.0 - 1.0)
 * @param intensityRight the intensity of erasure for the right side of the line (a float from 0.0 - 1.0)
 */
- (instancetype)initWithLine:(NSString *)line
               intensityLeft:(float)intensityLeft
             intensityCenter:(float)intensityCenter
              intensityRight:(float)intensityRight;

/**
 * @discussion this method allows you to change the left, center, and right intensity values for the erasable line
 */
- (void)setIntensityLeft:(float)intensityLeft intensityCenter:(float)intensityCenter intensityRight:(float)intensityRight;

- (NSString *)description;

@end

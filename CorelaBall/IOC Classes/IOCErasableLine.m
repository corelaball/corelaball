//
//  IOCErasableLine.m
//  Week7CodingChallengeSolution
//
//  Created by Abraham Avnisan on 3/15/17.
//  Copyright © 2017 Abraham Avnisan. All rights reserved.
//

#import "IOCErasableLine.h"
#import "IOCErasableWord.h"

@interface IOCErasableLine ()

@property (nonatomic) float intensityLeft;
@property (nonatomic) float intensityCenter;
@property (nonatomic) float intensityRight;

@end

@implementation IOCErasableLine

- (NSString *)line
{
    return  [self description];
}

- (instancetype)initWithLine:(NSString *)line
               intensityLeft:(float)intensityLeft
             intensityCenter:(float)intensityCenter
              intensityRight:(float)intensityRight
{
    
    // get a new erasableline from the factory (so we can return it later)
    IOCErasableLine *myErasableLine = [[IOCErasableLine alloc] init];
    
    // get words in the original line as an NSString*
    NSArray *wordsInLineOriginal = [line componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    // create an empty array to store our word objects after we've made sure to adjust
    // for 'words' that are just empty spaces
    NSMutableArray *wordsInLine = [[NSMutableArray alloc] init];
    
    // iterate over each word in the original, check it, then add the appropriate thing to
    // our new array
    for (NSString *word in wordsInLineOriginal) {
        // THIS KIND OF FOR LOOP IS CALLED OBJECTIVE-C FAST ENUMERATION
        // it is a faster way to iterate over an array
        
        // we want to retain the original line lengths, so we have to adjust for words that are just emptpy spaces
        if (![word isEqualToString:@""]) {
            [wordsInLine addObject:word];
        } else {
            [wordsInLine addObject:@" "];
        }
    }
    
    // set our wordsAsStringArray property
    myErasableLine.wordsAsStringArray = wordsInLine;
    
    // here we set our intensities, which in turn creates our erasable words
    [myErasableLine setIntensityLeft:intensityLeft intensityCenter:intensityCenter intensityRight:intensityRight];
    
    return myErasableLine;
}

- (void)setIntensityLeft:(float)intensityLeft intensityCenter:(float)intensityCenter intensityRight:(float)intensityRight
{
    _intensityLeft = intensityLeft;
    _intensityCenter = intensityCenter;
    _intensityRight = intensityRight;
    
    // each time the intenities are set, we re-erase our words
    NSArray *erasableWords = [self returnArrayOfErasableWords];
    self.words = erasableWords;
}

// HELPER METHODS
- (NSArray *)returnArrayOfErasableWords
{
    // we use a helper function to get an array of intensities for each word in the line
    NSArray *intensities = [self returnArrayOfIntensitiesWithIntensityLeft:_intensityLeft intensityCenter:_intensityCenter intensityRight:_intensityRight forNWords:(int)[self.wordsAsStringArray count]];
    
    // make sure the number of intensities matches the number of words in the line
    if ([self.wordsAsStringArray count] != [intensities count]) {
        NSLog(@"array length does not match!");
        exit(EXIT_FAILURE);
    }
    
    // here we get a new mutable array from the factory
    NSMutableArray *erasableWords = [[NSMutableArray alloc] init];
    
    // now we iterate over each word in the line and create an erasable word object for each word in the line,
    // then add it to our array of erasable words
    for (NSUInteger i = 0; i < [self.wordsAsStringArray count]; i++) {
        
        // we know that our intensities array matches our wordInLine array
        // so we can use the same index to pull out our intensity
        NSNumber *thisIntensity = [intensities objectAtIndex:i];
        
        float intensity = [thisIntensity floatValue];
        
        IOCErasableWord *thisErasableWord = [[IOCErasableWord alloc] initWithWord:[self.wordsAsStringArray objectAtIndex:i] andIntensity:intensity];
        
        [erasableWords addObject:thisErasableWord];
        
    }
    
    return erasableWords;
}

- (NSArray *)returnArrayOfIntensitiesWithIntensityLeft:(float)intensityLeft
                                       intensityCenter:(float)intensityCenter
                                        intensityRight:(float)intensityRight
                                             forNWords:(int)numWords
{
    
    // this helper method maps the intensities assigned to the line (left, center and right)
    // onto the words in the line.  It returns an array of intensities that is the same
    // length as the number of words in the line.
    
    if (numWords == 0) {
        NSLog(@"ERROR! No words to return intensities for!!");
        exit(101);
    }
    
    // create the array we will return
    NSMutableArray *intensities = [[NSMutableArray alloc] init];
    
    // calc differences between intensitites
    float differenceCenterToLeft = intensityCenter - intensityLeft;
    float differenceRightToCenter = intensityRight - intensityCenter;
    
    // add the first intensity to the array
    [intensities addObject:[NSNumber numberWithFloat:intensityLeft]];
    
    if (numWords % 2 == 0) {
        
        // NUMBER IS EVEN
        
        float spreadPerHalfPointLeft = differenceCenterToLeft / (numWords - 1);
        float spreadPerHalfPointRight = differenceRightToCenter / (numWords - 1);
        
        NSUInteger numSteps = (numWords / 2) - 1;
        
        // add first half
        for (NSUInteger i = 1; i <= numSteps; i++) {
            
            float spreadToAdd = (spreadPerHalfPointLeft * 2) * i;
            NSNumber *intensity = [NSNumber numberWithFloat:intensityLeft + spreadToAdd];
            
            // Make sure intensities are within range and if not force them
            if ([intensity floatValue] < 0) {
                intensity = [NSNumber numberWithFloat:0.0];
            } else if ([intensity floatValue] > 1.0) {
                intensity = [NSNumber numberWithFloat:1.0];
            }
            
            [intensities addObject:intensity];
            
        }
        
        // add second half
        for (NSUInteger i = 0; i <= numSteps; i++) {
            
            float spreadToAdd = (spreadPerHalfPointRight * 2) * i + spreadPerHalfPointRight;
            NSNumber *intensity = [NSNumber numberWithFloat:intensityCenter + spreadToAdd];
            
            // Make sure intensities are within range and if not force them
            if ([intensity floatValue] < 0) {
                intensity = [NSNumber numberWithFloat:0.0];
            } else if ([intensity floatValue] > 1.0) {
                intensity = [NSNumber numberWithFloat:1.0];
            }
            
            [intensities addObject:intensity];
        }
        
        
    } else {
        
        // NUMBER IS ODD
        if (numWords != 1) {
            
            float spreadPerPointLeft = differenceCenterToLeft / (int)(numWords / 2);
            float spreadPerPointRight = differenceRightToCenter / (int)(numWords / 2);
            
            NSUInteger numSteps = (int)(numWords / 2) - 1;
            
            // add first half
            for (NSUInteger i = 1; i <= numSteps; i++) {
                
                float spreadToAdd = spreadPerPointLeft * i;
                NSNumber *intensity = [NSNumber numberWithFloat:intensityLeft + spreadToAdd];
                
                // Make sure intensities are within range and if not force them
                if ([intensity floatValue] < 0) {
                    intensity = [NSNumber numberWithFloat:0.0];
                } else if ([intensity floatValue] > 1.0) {
                    intensity = [NSNumber numberWithFloat:1.0];
                }
                
                [intensities addObject:intensity];
            }
            
            // add center
            NSNumber *intensityCenterObj = [NSNumber numberWithFloat:intensityCenter];
            [intensities addObject:intensityCenterObj];
            
            // add second half
            for (NSUInteger i = 1; i <= numSteps; i++) {
                
                float spreadToAdd = spreadPerPointRight * i;
                NSNumber *intensity = [NSNumber numberWithFloat:intensityCenter + spreadToAdd];
                
                // Make sure intensities are within range and if not force them
                if ([intensity floatValue] < 0) {
                    intensity = [NSNumber numberWithFloat:0.0];
                } else if ([intensity floatValue] > 1.0) {
                    intensity = [NSNumber numberWithFloat:1.0];
                }
                [intensities addObject:intensity];
            }
            
            // add right
            NSNumber *intensityRightObj = [NSNumber numberWithFloat:intensityRight];
            [intensities addObject:intensityRightObj];
        }
    }
    return intensities;
}

- (NSString *)description
{
    NSString *description = @"";
    
    for (NSUInteger i = 0; i < [self.words count]; i++) {
        
        IOCErasableWord *word = [self.words objectAtIndex:i];
        
        if ([word.erased isEqualToString:@" "]) {
            description = [description stringByAppendingString:[NSString stringWithFormat:@"%@", word]];
        } else {
            description = [description stringByAppendingString:[NSString stringWithFormat:@"%@ ", word]];
        }
        
    }
    NSLog(@"%@", description);
    return description;
}

@end


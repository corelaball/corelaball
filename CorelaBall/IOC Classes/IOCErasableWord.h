//
//  IOCErasableWord.h
//  Week7CodingChallengeSolution
//
//  Created by Abraham Avnisan on 3/15/17.
//  Copyright © 2017 Abraham Avnisan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IOCMath.h"

@interface IOCErasableWord : NSObject

@property (nonatomic, strong) NSString *original;
@property (nonatomic, strong) NSString *erased;
@property (nonatomic) float intensity;

// DESIGNATED INITIALIZER
- (instancetype)initWithWord:(NSString *)word andIntensity:(float)intensityAsPercentage; // INTENSITY MUST BE FLOAT BETWEEN 0.0 AND 1.0

- (NSString *)description;

@end

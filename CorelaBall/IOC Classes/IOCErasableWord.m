//
//  IOCErasableWord.m
//  Week7CodingChallengeSolution
//
//  Created by Abraham Avnisan on 3/15/17.
//  Copyright © 2017 Abraham Avnisan. All rights reserved.
//

#import "IOCErasableWord.h"

@interface IOCErasableWord ()

@property (strong, nonatomic) IOCMath *mathHelper;

@end

@implementation IOCErasableWord

- (instancetype)initWithWord:(NSString *)word andIntensity:(float)intensityAsPercentage
{
    // make sure intensity is between 0 and 1
    if (intensityAsPercentage > 1.0 || intensityAsPercentage < 0.0) {
        NSLog(@"ERROR! Intensity must be a float between 0.0 and 1.0!!");
        exit(1);
    }
    
    // get a new ErasableWord object from the factory
    IOCErasableWord *myErasableWord = [[IOCErasableWord alloc] init];
    
    myErasableWord.mathHelper = [[IOCMath alloc] init];
    
    // set the original word property
    myErasableWord.original = word;
    
    // set the intensity.
    myErasableWord.intensity = intensityAsPercentage;
    
    // Because we override our intensity setter such that any
    // time it is called, it gets a new erased word, we don't
    // have to do anything else in our initializer - we can simply
    // return it
    
    return myErasableWord;
}

- (void)setIntensity:(float)intensity
{
    // set new intensity
    _intensity = intensity;
    
    // re-erase word with new intensity
    self.erased = [self erase:self.original withIntensity:self.intensity];
    
}

- (NSString *)erased
{
    
    if (!_erased) {
        
        _erased = [self erase:self.original withIntensity:self.intensity];
        
    }
    
    return _erased;
}

// helper method
-(NSString *)erase:(NSString *)wordToErase withIntensity:(float)intensity
{
    
    // map intensity to number or characters in the word
    NSUInteger numChars = [wordToErase length];
    float mapped = [self.mathHelper getMappedValueWithInput:intensity
                                                  minInput:0.0
                                                  maxInput:1.0
                                                 minOutput:0
                                                 maxOutput:numChars];
    NSUInteger numCharsToErase = (NSUInteger)mapped;
    
    // create a mutable array to store our random indices
    NSMutableArray *randomIndices = [[NSMutableArray alloc] init];
    
    // generate unique random numbers & store them in the array
    while ([randomIndices count] < numCharsToErase) {
        
        float hfRandomUpperBound = [wordToErase length] - .1;
        float randomFloat = [self.mathHelper getRandomFloatWithMinOutput:0 maxOutput:hfRandomUpperBound];
        NSUInteger randomIndex = (NSUInteger)randomFloat;
        NSNumber *randomIndexAsObject = [NSNumber numberWithInteger:randomIndex];
        
        // if randomIndexAsObject isn't ALREADY in the array, add it
        if (![randomIndices containsObject:randomIndexAsObject]) {
            
            [randomIndices addObject:randomIndexAsObject];
            
        }
    }
    
    NSString *erasedWord = @"";
    
    for (NSUInteger i = 0; i < [wordToErase length]; i++) {
        
        // create an NSNumber from this index
        NSNumber *thisIndex = [NSNumber numberWithInteger:i];
        
        if ([randomIndices containsObject:thisIndex]) {
            erasedWord = [erasedWord stringByAppendingString:@" "];
        } else {
            unichar thisChar = [wordToErase characterAtIndex:i];
            NSString *thisCharAsString = [NSString stringWithFormat:@"%C", thisChar];
            erasedWord = [erasedWord stringByAppendingString:thisCharAsString];
        }
        
    }
    return erasedWord;
    
}
- (NSString *)description
{
    return self.erased;
}

@end

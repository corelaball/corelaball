//
//  IOCImageAndTextView.h
//  MidtermDemo
//
//  Created by Abraham Avnisan on 2/14/17.
//  Copyright © 2017 Abraham Avnisan. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 * @class IOCImageAndTextViewAnimatedScroll
 *
 * @dicussion 	This class allows clients to configure an looping 
 *              scroll of images, which can be moved through swipe
 *              interaction. Clients configure the class by setting
 *              its imagesArray property.  Further configuration of 
 *              scroll speed and duration are possible though not
 *              required.
 *
 * @warning This class currently does NOT support text
 * @warning This class is under active development and does currently contain a few bugs
 */
@interface IOCImageAndTextViewAnimatedScroll : UIView <UIGestureRecognizerDelegate>

/**
 * @brief The array of UIImages to be used in the slide show.
 */
@property (strong, nonatomic) NSArray<UIImage *> *imagesArray;
/**
 * @brief How to display the image. The default value for this property is UIViewContentModeScaleAspectFill.
 */
@property (nonatomic) UIViewContentMode imageContentMode;
/**
 * @brief How far the images scroll when you swipe. Default value for this is 0.5.
 */
@property (nonatomic) float distanceScaleFactor;
/**
 * @brief How long the images scroll for after you swipe. Default value for this is 0.001.
 */
@property (nonatomic) float durationScaleFactor;


@end

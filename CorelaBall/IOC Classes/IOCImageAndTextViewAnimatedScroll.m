//
//  IOCImageAndTextView.m
//  MidtermDemo
//
//  Created by Abraham Avnisan on 2/14/17.
//  Copyright © 2017 Abraham Avnisan. All rights reserved.
//

#import "IOCImageAndTextViewAnimatedScroll.h"

@interface IOCImageAndTextViewAnimatedScroll ()


//@property (nonatomic) BOOL isPlaying;

// GENERAL
@property (nonatomic) BOOL configuredForImages;
@property (nonatomic) NSInteger itemIndexCurrent;
@property (nonatomic) NSInteger itemIndexPrevious;
@property (nonatomic) NSInteger itemIndexNext;

//@property (nonatomic) NSUInteger itemIndexImageView0;
//@property (nonatomic) NSUInteger itemIndexImageView1;
//@property (nonatomic) NSUInteger itemIndexImageView2;

@property (strong, nonatomic) NSTimer *timer;

// GESTURE RECOGNIZERS
//@property (strong, nonatomic) UITapGestureRecognizer *tapRecognizer;
//@property (strong, nonatomic) UISwipeGestureRecognizer *leftSwipeRecognizer;
//@property (strong, nonatomic) UISwipeGestureRecognizer *rightSwipeRecognizer;
// @property (strong, nonatomic) UILongPressGestureRecognizer *longPressGestureRecognizer;
@property (strong, nonatomic) UIPanGestureRecognizer *panGestureRecognizer;

// ANIMATION
@property (strong, nonatomic) NSMutableArray<NSValue *> *subviewFrames;
@property (strong, nonatomic) NSArray <UIImageView *>*subviews;
@property (nonatomic) float xTranslation;
@property (nonatomic) float xTranslationPrevious;
@property (nonatomic) BOOL movingRight;
@property (nonatomic) BOOL isAnimating;
@property (strong, nonatomic) NSMutableDictionary *viewPositionsDict;

@property (nonatomic) float distanceTranslatedDuringAnimation;
@property (nonatomic) float xOriginTranslationReference;

// STILL UNDER DEVELOPMENT
@property (strong, nonatomic) NSArray <NSString *> *textsArray;


@end

@implementation IOCImageAndTextViewAnimatedScroll

#pragma mark - accessor methods

@synthesize imagesArray = _imagesArray;

// GESTURE RECOGNIZERS - LAZY INSTANTIATION
- (UIPanGestureRecognizer *)panGestureRecognizer
{
    if (!_panGestureRecognizer) {
        _panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self
                                                                        action:@selector(panGestureRecognizedWithRecognizer:)];
    }
    return _panGestureRecognizer;
}
// IMAGE VIEW - LAZY INSTANTIATION
- (NSMutableArray<NSValue *> *)subviewFrames
{
    if (!_subviewFrames) {
        _subviewFrames = [[NSMutableArray alloc] initWithCapacity:3];
    }
    return _subviewFrames;
}
- (NSMutableDictionary *)viewPositionsDict
{
    if (!_viewPositionsDict) {
        _viewPositionsDict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                              [NSNumber numberWithFloat:0], @"left",
                              [NSNumber numberWithFloat:0], @"right",
                              [NSNumber numberWithFloat:0], @"center", nil];
    }
    return _viewPositionsDict;
}
// SETTERS
- (void)setItemIndexCurrent:(NSInteger)itemIndexCurrent
{
    _itemIndexCurrent       = [self returnAdjustedItemIndexForIndex:itemIndexCurrent];
    self.itemIndexNext      = [self returnAdjustedItemIndexForIndex:_itemIndexCurrent + 1];
    self.itemIndexPrevious  = [self returnAdjustedItemIndexForIndex:_itemIndexCurrent - 1];
    NSLog(@"setting item index prev to %ld current to %ld next to %ld ", (long)self.itemIndexPrevious, (long)_itemIndexCurrent, (long)self.itemIndexNext);
}
- (void)setTextsArray:(NSArray<NSString *> *)textsArray
{
    NSLog(@"ERROR! IOCImageAndTextViewAnimatedScroll objects do not yet support text. Please be patient and wait for an update :)");
    exit(EXIT_FAILURE);
}
- (void)setImagesArray:(NSArray <UIImage *>*)imagesArray
{
    if (imagesArray.count < 3) {
        NSLog(@"ERROR! IOCImageAndTextViewAnimatedScroll objects must have at LEAST three images! Try setting the imagesArray property to an array that has three or more items");
        exit(EXIT_FAILURE);
    }
    
    _imagesArray = imagesArray;
    self.configuredForImages = YES;
    
    for (NSUInteger i = 0; i < self.subviews.count; i++) {
        UIImageView *thisImageView = self.subviews[i];
        [thisImageView setImage:imagesArray[i]];
    }
    
    self.itemIndexCurrent = 0;
    
    [self.subviews[0] setImage:self.imagesArray[self.itemIndexPrevious]];
    [self.subviews[1] setImage:self.imagesArray[self.itemIndexCurrent]];
    [self.subviews[2] setImage:self.imagesArray[self.itemIndexNext]];
    
}
- (void)setImageContentMode:(UIViewContentMode)imageContentMode
{
    
    _imageContentMode = imageContentMode;
    
    for (UIImageView *imageView in self.subviews) {
        imageView.contentMode = imageContentMode;
    }
}
// ACCESSOR HELPER METHOD
- (NSInteger)returnAdjustedItemIndexForIndex:(NSInteger)index
{
    NSInteger maxIndex = self.configuredForImages ? self.imagesArray.count - 1 : self.textsArray.count - 1;
    NSInteger adjustedIndex;
    
    if (index > maxIndex) {
        adjustedIndex = 0;
    } else if (index < 0) {
        adjustedIndex = maxIndex;
    } else {
        adjustedIndex = index;
    }
    NSLog(@"returning adjusted index %li for index %li", (long)adjustedIndex, (long)index);
    return adjustedIndex;
}
#pragma mark - setup
- (void)setup
{
    [self setClipsToBounds:YES];
    
    // CONFIGURE INITIAL STATE
    self.isAnimating = NO;
    self.distanceScaleFactor = 0.5;
    self.durationScaleFactor = 0.001;
    self.contentMode = UIViewContentModeScaleAspectFill;
    
    // ADD PAN GESTURE RECOGNIZER
    [self addGestureRecognizer:self.panGestureRecognizer];
    // [self addGestureRecognizer:self.longPressGestureRecognizer];
    
    // SETUP IMAGE VIEW
    [self setupSubViews];
    [self updateViewPositionsDictionary];
    
    // INIT ANIMATION VARS
    self.xTranslation = 0;
    self.xTranslationPrevious = 0;
    
}
- (void)setupSubViews
{
    NSMutableArray *subviewsLocal = [[NSMutableArray alloc] init];
    
    float width = self.bounds.size.width;
    float height = self.bounds.size.height;
    float xOrigin = self.bounds.origin.x;
    float yOrigin = self.bounds.origin.y;
    
    // >>>>>>> TEMP DEBUG >>>>>>>
    self.configuredForImages = YES;
//    self.layer.borderColor = [UIColor blackColor].CGColor;
//    self.layer.borderWidth = 2.0;
    
    if (self.configuredForImages) {
        
        for (NSUInteger i = 0; i < 3; i++) {
            
            xOrigin = (i * width) - width;
            CGRect frame = CGRectMake(xOrigin, yOrigin, width, height);
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
            [imageView setClipsToBounds:YES];
            
            // >>>>>>> TEMP DEBUG >>>>>>>
            //            if (i == 0) {
            //                imageView.backgroundColor = [UIColor redColor];
            //            } else if (i == 1) {
            //                imageView.backgroundColor = [UIColor greenColor];
            //            } else if (i == 2) {
            //                imageView.backgroundColor = [UIColor blueColor];
            //            }
            [self addSubview:imageView];
            [subviewsLocal addObject:imageView];
            [self.subviewFrames addObject:[NSValue valueWithCGRect:imageView.frame]];
        }
        
    } else {
        
    }
    
    self.subviews = [NSArray arrayWithArray:subviewsLocal];
}
#pragma mark - update
- (void)updateImageViewWithIndex:(NSUInteger)subviewIndex
{
    NSLog(@"%s with subViewIndex %lu", __PRETTY_FUNCTION__, (long unsigned)subviewIndex);
    for (NSString *key in self.viewPositionsDict) {
        
        NSNumber *indexObj = self.viewPositionsDict[key];
        NSUInteger subviewIndexLocal = [indexObj unsignedIntegerValue];
        
        if (subviewIndexLocal == subviewIndex) {
            
            NSUInteger imagesArrayIndex;
            
            if ([key isEqualToString:@"left"]) {
                imagesArrayIndex = self.itemIndexPrevious;
            } else if ([key isEqualToString:@"center"]) {
                imagesArrayIndex = self.itemIndexCurrent;
            } else {
                imagesArrayIndex = self.itemIndexNext;
            }
            
            NSLog(@"key: %@", key);
            NSLog(@"setting subviews[%lu] to imagesArray[%lu]", (long unsigned)subviewIndexLocal, (long unsigned)imagesArrayIndex);
            [self.subviews[subviewIndexLocal] setImage:self.imagesArray[imagesArrayIndex]];
            
        }
    }
}
- (void)updateSubviewFramesArray
{
    // NSLog(@"%s", __PRETTY_FUNCTION__);
    
    if (self.isAnimating) {
        for (NSUInteger i = 0; i < self.subviewFrames.count; i++) {
            UIImageView *thisSubview = (UIImageView *)self.subviews[i];
            CGRect currentRect = [thisSubview.layer presentationLayer].frame;
            self.subviewFrames[i] = [NSValue valueWithCGRect:currentRect];
            
            if (i == 0) {
                self.distanceTranslatedDuringAnimation += fabs(self.xOriginTranslationReference - currentRect.origin.x);
            }
            
        }
    } else {
        for (NSUInteger i = 0; i < self.subviewFrames.count; i++) {
            UIImageView *thisSubview = (UIImageView *)self.subviews[i];
            // CGRect currentRect = [thisSubview.layer presentationLayer].frame;
            CGRect currentRect = thisSubview.frame;
            self.subviewFrames[i] = [NSValue valueWithCGRect:currentRect];
        }
    }
    
    [self adjustViewPosition];
    [self updateViewPositionsDictionary];
    
    // [self updateImageViewContents];
    
    // log position of first frame
    NSString *prefix;
    if (self.isAnimating) {
        prefix = [NSString stringWithFormat:@"    ANIMATING TO RIGHT: %i - ", self.movingRight];
    } else {
        prefix = [NSString stringWithFormat:@"NOT ANIMATING TO RIGHT: %i - ", self.movingRight];
    }
    
    
//    for (NSUInteger i = 0; i < self.subviewFrames.count; i++) {
//        
//        CGRect currentRect = [self.subviewFrames[i] CGRectValue];
//        NSString *message = [prefix stringByAppendingFormat:@"xOrigin[%lu] = %.2f", (long unsigned)i, currentRect.origin.x];
//        NSLog(@"%@", message);
//    }
    
    
}
- (void)adjustViewPosition
{
    float boundsWidth = self.bounds.size.width;

    if (!self.movingRight) {
        // MOVING LEFT --> ADVANCING
        for (NSUInteger i = 0; i < self.subviewFrames.count; i++) {
            
            float thisXOrigin = [self.subviewFrames[i] CGRectValue].origin.x;
            
            if (thisXOrigin < boundsWidth * -1) {

                UIImageView *thisImageView = self.subviews[i];
                CGRect currentFrame = thisImageView.frame;
                
                thisImageView.frame = CGRectMake(currentFrame.origin.x + boundsWidth * 3,
                                                 currentFrame.origin.y,
                                                 currentFrame.size.width,
                                                 currentFrame.size.height);
                
                NSLog(@"setting originX for subviewFrame[%lu] to %f", (long unsigned)i, currentFrame.origin.x + boundsWidth * 3);
                
                // UPDATE ITEM INDEX
                self.itemIndexCurrent++;
                // UPDATE DICTIONARY
                [self updateViewPositionsDictionary];
                // UPDATE THIS IMAGE VIEW'S DISPLAYED IMAGE
                [self updateImageViewWithIndex:i];
            }
        }
    } else {
        // MOVING RIGHT --> GOING BACKWARDS
        for (NSUInteger i = 0; i < self.subviewFrames.count; i++) {
            
            float thisXOrigin = [self.subviewFrames[i] CGRectValue].origin.x;
            
            if (thisXOrigin > boundsWidth) {
                
                UIImageView *thisImageView = self.subviews[i];
                CGRect currentFrame = thisImageView.frame;
                
                thisImageView.frame = CGRectMake(currentFrame.origin.x - boundsWidth * 3,
                                                 currentFrame.origin.y,
                                                 currentFrame.size.width,
                                                 currentFrame.size.height);
                
                NSLog(@"setting originX for subviewFrame[%lu] to %f", (long unsigned)i, currentFrame.origin.x - boundsWidth * 3);

                // UPDATE ITEM INDEX
                self.itemIndexCurrent--;
                // UPDATE DICTIONARY
                [self updateViewPositionsDictionary];
                // UPDATE THIS IMAGE VIEW'S DISPLAYED IMAGE
                [self updateImageViewWithIndex:i];
            }
        }
    }
}
- (void)updateViewPositionsDictionary
{
    // NSLog(@"updatingViewPositionsDictionary with ANIMATION: %i", self.isAnimating);
    
    float originXMin = 1000;
    float originXCenter = 0;
    float originXMax = -1000;
    
    
    // CALCULATE LOW AND HIGH
    for (NSUInteger i = 0; i < self.subviews.count; i++) {
        
        UIImageView *imageView = self.subviews[i];
        CGRect currentRect = imageView.frame;
        
//        if (self.isAnimating) {
//            // currentRect = [imageView.layer presentationLayer].frame;
//            currentRect = imageView.frame;
//        } else {
//            currentRect = imageView.frame;
//        }
        
        
        if (currentRect.origin.x < originXMin) {
            originXMin = currentRect.origin.x;
            NSLog(@"setting dictionary left: %lu", (long unsigned)i);
            [self.viewPositionsDict setValue:[NSNumber numberWithUnsignedInteger:i] forKey:@"left"];
        }
        if (currentRect.origin.x > originXMax) {
            originXMax = currentRect.origin.x;
            NSLog(@"setting dictionary right: %lu", (long unsigned)i);
            [self.viewPositionsDict setValue:[NSNumber numberWithUnsignedInteger:i] forKey:@"right"];
        }
        
        NSLog(@"currentOriginX: %.2f | originXMin: %.2f | originXCenter: %.2f | originXMax: %.2f", currentRect.origin.x, originXMin, originXCenter, originXMax);
        
    }
    
    // CALCULATE CENTER
    for (NSUInteger i = 0; i < self.subviews.count; i++) {
        
        UIImageView *imageView = self.subviews[i];
        CGRect currentRect = imageView.frame;
        
        //        if (self.isAnimating) {
        //            // currentRect = [imageView.layer presentationLayer].frame;
        //            currentRect = imageView.frame;
        //        } else {
        //            currentRect = imageView.frame;
        //        }
        
        if (currentRect.origin.x < originXMax && currentRect.origin.x > originXMin) {
            originXCenter = currentRect.origin.x;
            NSLog(@"setting dictionary center: %lu", (long unsigned)i);
            [self.viewPositionsDict setValue:[NSNumber numberWithUnsignedInteger:i] forKey:@"center"];
        }
        
    }
    
    // NSLog(@"min: %.2f | center: %.2f | max: %.2f", originXMin, originXCenter, originXMax);
    
}
#pragma mark - interactivity
- (void)stopAnimation
{
//    if (self.isAnimating || !self.isAnimating) {
//        
//        [UIView animateWithDuration:0.0
//                              delay:0.0
//                            options:UIViewAnimationOptionBeginFromCurrentState
//                         animations:^{
//                             for (NSUInteger i = 0; i < self.subviewFrames.count; i++) {
//                                 
//                                 UIImageView *thisImageView = (UIImageView *)self.subviews[i];
//                                 thisImageView.frame = ((CALayer *)thisImageView.layer.presentationLayer).frame;
//                                 
//                             }
//                         }
//                         completion:^(BOOL finished){
//                             if (finished) {
//                                 // NSLog(@"FINISHED");
//                                 // self.xTranslationPrevious += distanceToTranslate;
//                                 [self.timer invalidate];
//                                 self.isAnimating = NO;
//                                 for (NSUInteger i = 0; i < self.subviewFrames.count; i++) {
//                                     UIImageView *thisSubview = (UIImageView *)self.subviews[i];
//                                     CGRect currentRect = [thisSubview.layer presentationLayer].frame;
//                                     self.subviewFrames[i] = [NSValue valueWithCGRect:currentRect];
//                                     // NSLog(@"xOrigin[%lu] = %f", (long unsigned)i, currentRect.origin.x);
//                                 }
//                             }
//                         }
//         ];
//    }
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    //    NSLog(@"%s", __PRETTY_FUNCTION__);
    //    NSLog(@"hold");
    //    if (self.isAnimating) {
    //        NSLog(@"TAP RECOGNIZED, GOING TO STOP ANIMATION NOW");
    //        [self stopAnimation];
    //    }
}
- (void)panGestureRecognizedWithRecognizer:(UIPanGestureRecognizer *)recognizer
{
    
    CGPoint translation = [recognizer translationInView:self];
    CGPoint velocity = [recognizer velocityInView:self];
    
    // NSLog(@"xTranslatoin:%.2f xVelocity: %.2f", translation.x, velocity.x);
    
    self.movingRight = velocity.x > 0 ? YES : NO;
    
    // BOOL stateBegan = recognizer.state == UIGestureRecognizerStateBegan ? YES : NO;
    
    NSLog(@"%s with stateBegan: %li", __PRETTY_FUNCTION__, (long)recognizer.state);
    
    if (recognizer.state == UIGestureRecognizerStateBegan && self.isAnimating) {
        
        
    } else if (recognizer.state == UIGestureRecognizerStateEnded) {
        
        self.xTranslationPrevious += translation.x;
        
        if (velocity.x > 80 || velocity.x < -80) {
            BOOL animating = YES;
            if (animating) {
                
                // self.userInteractionEnabled = NO;
                self.isAnimating = YES;
                self.userInteractionEnabled = NO;
                self.distanceTranslatedDuringAnimation = 0;
                self.xOriginTranslationReference = self.subviews[0].frame.origin.x;
                
                float distanceToTranslate = velocity.x * self.distanceScaleFactor;
                float animationDuration = fabs(velocity.x * self.durationScaleFactor);

                // NSLog(@"ANIMATING with duration: %.2f | distanceToTranslate of %.2f", animationDuration, distanceToTranslate);
                
                self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0/60.0
                                                              target:self
                                                            selector:@selector(updateSubviewFramesArray)
                                                            userInfo:nil
                                                             repeats:YES];
                
                [UIView animateWithDuration:animationDuration delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
                    
                    for (NSUInteger i = 0; i < self.subviewFrames.count; i++) {
                        
                         CGRect thisFrame = [self.subviewFrames[i] CGRectValue];
                        UIImageView *thisImageView = (UIImageView *)self.subviews[i];
                        
                        // THIS IS A NEW TRY
                        // thisImageView.frame = CGRectMake(thisFrame.origin.x + distanceToTranslate, thisFrame.origin.y, thisFrame.size.width, thisFrame.size.height);
                        
                        // THIS WORKS
                                               thisImageView.transform = CGAffineTransformTranslate(thisImageView.transform, distanceToTranslate, 0);
                    
                    }
                    
                } completion:^(BOOL finished) {
                    if (finished) {
                        self.xTranslationPrevious += distanceToTranslate;
                        [self.timer invalidate];
                        self.isAnimating = NO;
                        self.userInteractionEnabled = YES;
                        for (NSUInteger i = 0; i < self.subviewFrames.count; i++) {
                            UIImageView *thisSubview = (UIImageView *)self.subviews[i];
                            CGRect currentRect = [thisSubview.layer presentationLayer].frame;
                            self.subviewFrames[i] = [NSValue valueWithCGRect:currentRect];
                            // NSLog(@"xOrigin[%lu] = %f", (long unsigned)i, currentRect.origin.x);
                        }

                        [self updateViewPositionsDictionary];
                    }
                }];
            }
        }
        
        
    } else {
        
        float amountToTranslate = self.xTranslationPrevious + translation.x;
        
        // NSLog(@"amountToTranslate = %.2f", amountToTranslate);
        
        CGAffineTransform t = CGAffineTransformIdentity;
        t = CGAffineTransformTranslate(t, amountToTranslate, 0);
        
        for (UIImageView *imageView in self.subviews) {
            
            imageView.transform = t;
            [self updateSubviewFramesArray];
        }
    }
}
#pragma mark - inherited methods
- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

@end

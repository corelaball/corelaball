//
//  IOCMoveWithGesture.h
//  transformWithSliders
//
//  Created by Abraham Avnisan on 4/11/17.
//  Copyright © 2017 Abraham Avnisan. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol IOCMoveWithGestureDelegate;

/**
 * @class IOCMoveWithGesture
 * @discussion  This is a subclass of the UIView class. It moves, scales and rotates
 *              in response to pan, pinch and rotate gestures. It can also display an 
 *              image via its image property. In order to properly initialize an
 *              instance of this class, you must *FIRST* drag a generic UIView onto your
 *              storyboard, and set its custom class to IOCMoveWithGesture. *SECOND*,
 *              you must make declare that your ViewController class conforms to the 
                IOCMoveWithGestureDelegate protocol by changing this line in your ViewController.h file
 *              "@interface ViewController : UIViewController" to
 *              "@interface ViewController : UIViewController <IOCMoveWithGestureDelegate>".
 *              *THIRD*, you must add these two lines of code to your ViewController's setup method:
 *              self.gestureView.delegate = self;
 *              [self.gestureView setup];
 *              *FOURTH*, you must add an update method to your ViewController, which will get called  
 *              every time the position, scale or rotation of this object changes.
 *
 */
@interface IOCMoveWithGesture : UIView

@property (strong, nonatomic) id<IOCMoveWithGestureDelegate> delegate;

/** @brief if you'd like to display an image, set this property */
@property (strong, nonatomic) UIImage *image;

/** @brief the object's current x position */
@property (nonatomic) float translateX;
/** @brief the object's current y position */
@property (nonatomic) float translateY;
/** @brief the object's current scale */
@property (nonatomic) float scale;
/** @brief the object's current rotation */
@property (nonatomic) float rotationDegrees;

/** @brief this method must be called from your viewcontroller's setup method to properly initialize this object */
- (void)setup;

/** @brief this method prints the objects position, scale and rotation to the console */
- (void)logStats;

@end

// DON'T WORRY ABOUT THIS
// ________________________________________________________________________________________________ //

@protocol IOCMoveWithGestureDelegate <NSObject>
@optional
- (void)update;
@end
// ________________________________________________________________________________________________ //

//
//  IOCMoveWithGesture.m
//  transformWithSliders
//
//  Created by Abraham Avnisan on 4/11/17.
//  Copyright © 2017 Abraham Avnisan. All rights reserved.
//

#import "IOCMoveWithGesture.h"
#import "IOCMath.h"

@interface IOCMoveWithGesture ()

@property (strong, nonatomic) IOCMath *mathHelper;

@property (nonatomic) CGPoint translatePointCurrent;
@property (nonatomic) CGPoint translatePointPrevious;

@property (nonatomic) float translateXPrev;
@property (nonatomic) float translateYPrev;

@property (nonatomic) float rotationRadians;
@property (nonatomic) float rotationCurrent;
@property (nonatomic) float rotationPrevious;

@property (nonatomic) float scaleCurrent;
@property (nonatomic) float scalePrevious;

@property (strong, nonatomic) UIView *gestureView;

@property (strong, nonatomic) UIImageView *imageView;

@end

@implementation IOCMoveWithGesture
#pragma mark - lazy instanatiation & accessor methods
- (float)rotationDegrees
{
    return [self.mathHelper convertRadiansToDegrees:self.rotationRadians];
}
- (UIImageView *)imageView
{
    if (!_imageView) {
        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height)];
        [self addSubview:_imageView];
    }
    return _imageView;
}
- (void)setImage:(UIImage *)image
{
    _image = image;
    [self.imageView setImage:image];
}
#pragma mark - setup
- (void)setup
{
    self.mathHelper = [[IOCMath alloc] init];
    
    self.scalePrevious = 1.0;
    self.scaleCurrent = 1.0;
    
    self.rotationPrevious = 0;
    self.rotationCurrent = 0;
    
    [self setupGestureView];
}
- (void)setupGestureView
{
    self.gestureView = [[UIView alloc] initWithFrame:self.superview.frame];
    self.gestureView.backgroundColor = [UIColor clearColor];
    [self.superview addSubview:self.gestureView];
    
    UIPinchGestureRecognizer *pinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(userDidPinch:)];
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(userDidPan:)];
    UIRotationGestureRecognizer *rotate = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(userDidRotate:)];

    [self.gestureView addGestureRecognizer:pinch];
    [self.gestureView addGestureRecognizer:pan];
    [self.gestureView addGestureRecognizer:rotate];
    

}
#pragma mark - gesture & transform
- (void)userDidPinch:(UIPinchGestureRecognizer *)sender
{
    
    self.scaleCurrent = sender.scale;
    [self updateTransform];
    
    if (sender.state == UIGestureRecognizerStateEnded) {
        self.scalePrevious = self.scale;
        self.scaleCurrent = 1.0;
    }
    
}
- (void)userDidRotate:(UIRotationGestureRecognizer *)sender
{
    
    self.rotationCurrent = sender.rotation;
    [self updateTransform];
    
    if (sender.state == UIGestureRecognizerStateEnded) {
        self.rotationPrevious = self.rotationRadians;
        self.rotationCurrent = 0.0;
    }
    
}
- (void)userDidPan:(UIPanGestureRecognizer *)sender
{
    
    
    if (sender.state == UIGestureRecognizerStateBegan) {
        self.translatePointCurrent = [sender locationInView:self.superview];
        self.translatePointPrevious = self.translatePointCurrent;
    }
    
    self.translatePointCurrent = [sender locationInView:self.superview];
    [self updateTransform];

    if (sender.state == UIGestureRecognizerStateEnded) {
        self.translatePointPrevious = self.translatePointCurrent;
        self.translateXPrev = self.translateX;
        self.translateYPrev = self.translateY;
    }
    
}

- (void)updateTransform
{
    // calculate translates
    self.translateX = self.translateXPrev + self.translatePointCurrent.x - self.translatePointPrevious.x;
    //    NSLog(@"translateX = self.translatePointCurrent.x - self.translatePointPrevious.x");
    //    NSLog(@"%.4f = %.4f - %.4f", self.translateX, self.translatePointCurrent.x, self.translatePointPrevious.x);
    
    self.translateY = self.translateYPrev + self.translatePointCurrent.y - self.translatePointPrevious.y;
    
    
    // calculate rotation
    self.rotationRadians = self.rotationPrevious + self.rotationCurrent;
    //    NSLog(@"self.rotation = self.rotationPrevious + self.rotatoinCurrent");
    //    NSLog(@"%.4f = %.4f + %.4f", self.rotationRadians, self.rotationPrevious, self.rotationCurrent);
    
    // calculate scale
    self.scale = self.scalePrevious * self.scaleCurrent;
    //    NSLog(@"self.scale = self.scalePrevious * self.scaleCurrent");
    //    NSLog(@"%.4f = %.4f * %.4f", self.scale, self.scalePrevious, self.scaleCurrent);
    
    CGAffineTransform transformSubview = CGAffineTransformIdentity;
    transformSubview = CGAffineTransformTranslate(transformSubview, self.translateX, self.translateY);
    transformSubview = CGAffineTransformScale(transformSubview, self.scale, self.scale);
    transformSubview = CGAffineTransformRotate(transformSubview, self.rotationRadians);

    self.transform = transformSubview;
    
    [self.delegate update];

    
}
- (void)logStats
{
    NSLog(@"translateX: %.2f | translateY:%.2f | rotation: %.2f | scale: %.2f", self.translateX, self.translateY, self.rotationDegrees, self.scale);
}
@end
